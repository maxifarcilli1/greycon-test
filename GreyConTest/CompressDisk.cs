﻿using System;

namespace GreyConTest
{
    public class CompressDisk
    {
        public static int getMinDiskNecessary(int[] used, int[] total)
        {
            int totalIndex = 0, usageSpace = 0;
            int it = 0;
            
            // Ordeno el array total con el algoritmo de quicksort y me quedo con el disco mas grande.
            quickSort(total, 0, total.Length - 1);
            int totalSpace = total[total.Length - 1];
            
            do
            {

                usageSpace = usageSpace + used[it];
                if (totalSpace < usageSpace)
                {
                    totalIndex++;
                    totalSpace = totalSpace + total[total.Length - 1 - totalIndex];
                }
                it++;
            } while (it < used.Length);

            return totalIndex + 1;
        }

        private static void quickSort(int[] array, int low, int high)
        {
            if (low < high)
            {
                // Select pivot position and put all the elements smaller 
                // than pivot on left and greater than pivot on right
                int pi = partition(array, low, high);

                // Sort the elements on the left of pivot
                quickSort(array, low, pi - 1);

                // Sort the elements on the right of pivot
                quickSort(array, pi + 1, high);
            }
        }

        // Function to partition the array on the basis of pivot element
        private static int partition(int[] array, int low, int high)
        {

            // Select the pivot element
            int pivot = array[high];
            int i = (low - 1);

            // Put the elements smaller than pivot on the left and 
            // greater than pivot on the right of pivot
            int tmp1, tmp2;
            for (int j = low; j < high; j++)
            {
                if (array[j] <= pivot)
                {
                    i++;
                    tmp1 = array[i];
                    array[i] = array[j];
                    array[j] = tmp1;
                }
            }
            tmp2 = array[i + 1];
            array[i + 1] = array[high];
            array[high] = tmp2;
            return (i + 1);
        }
    }
}
