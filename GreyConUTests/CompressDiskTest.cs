using GreyConTest;
using NUnit.Framework;


namespace GreyConUTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            
        }

        [Test]
        public void GenericTest()
        {
            int[] used = { 10, 12, 30, 1}; // uso total = 53
            int[] total = { 15, 22, 31, 3 }; // 31 + 22 = total_used => 2 discos son necesarios
            Assert.AreEqual(2, CompressDisk.getMinDiskNecessary(used, total));
        }

        [Test]
        public void MinNotFullTest()
        {
            int[] used = { 10 };
            int[] total = { 1000 };
            Assert.AreEqual(1, CompressDisk.getMinDiskNecessary(used, total));
        }

        [Test]
        public void MinFullTest()
        {
            int[] used = { 1000 };
            int[] total = { 1000 };
            Assert.AreEqual(1, CompressDisk.getMinDiskNecessary(used, total));
        }

        [Test]
        public void MaxFullTest()
        {
            int[] used = new int[50];
            int[] total = new int[50];
            for (int i = 0; i < used.Length; i++)
            {
                used[i] = 1000;
                total[i] = 1000;
            }

            Assert.AreEqual(50, CompressDisk.getMinDiskNecessary(used, total));
        }
        
    }
}